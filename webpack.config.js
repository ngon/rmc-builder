const HtmlPlugin = require('html-webpack-plugin');

const path = require('path');
const webpack = require('webpack');

const port = 4000;
module.exports = {
  //devtool: 'eval',
  //debug: true,
  port,
  entry: {
    index: [
      `webpack-dev-server/client?http://0.0.0.0:${port}`,
      'webpack/hot/only-dev-server',
      './src/index'
    ],
    worker: [
      './src/index'
    ]
  },
  devServer: {
    hot: true,
    historyApiFallback: {
      index: 'index.html',
      rewrites: [
        { from: /\/\w*\.js$/, to: c => c.match[0] },
        { from: /.*/, to: '/index.html'}
      ]
    },
    publicPath: '/'
  },
  output: {
    publicPath: '/',
    filename: '[name].js',
    path: __dirname + '/bin'
  },
  resolve: {
    modulesDirectories: [
      'src',
      'node_modules'
    ],
    extensions: ['', '.js', '.jsx', '.json', '.css', '.less']
  },
  module: {
    loaders: [
      {test: /src.*\.js[x*]?$/, loaders: ['babel-loader']},
      {test: /src.*\.json$/, loaders: ['json-loader']},
      {test: /src.*\.less$/,  loader: 'style-loader!css-loader!less-loader'},
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" }
    ]
  },
  externals: {
    'aws-sdk': 'AWS'
  },
  plugins: [
    new HtmlPlugin({ template: 'build/index.html', filename: 'index.html', excludeChunks:['worker'] }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
};
