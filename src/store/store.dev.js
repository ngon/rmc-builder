import { createStore as create, applyMiddleware, compose } from 'redux';
import { persistState } from 'redux-devtools';
import DevTools from 'components/devtools';

import reducer from '../reducers/index';
import promise from 'redux-promise';

const withMiddleware = applyMiddleware(promise);

const createDebugStore = compose(
  DevTools.instrument(),
  persistState(
    window.location.href.match(
      /[?&]debug_session=([^&]+)\b/
    )
  )
)(create);


export const createStore = initialState =>
  withMiddleware(createDebugStore)(reducer, initialState);

export const store = createStore();

if (module.hot) {
  module.hot.accept('../reducers', () =>
    store.replaceReducer(require('../reducers').default));
}
