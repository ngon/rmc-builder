import { createStore as create, applyMiddleware, compose } from 'redux';

import reducer from '../reducers';
import promise from 'redux-promise';

const withMiddleware = applyMiddleware(promise);

export const createStore = initialState =>
  withMiddleware(create)(reducer, initialState);

export const store = createStore();
