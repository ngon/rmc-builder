require('react-tap-event-plugin')();
require('font-awesome-webpack');

import {} from './style/index.less'

import { createElement } from 'react'
import { render } from 'react-dom'

import { store } from './store'
import root from 'components/root'

// HTML5 Equivalent of $(document).ready
document.addEventListener('DOMContentLoaded', () =>
  render(createElement(root, {store}), document.getElementById('root'))
);
