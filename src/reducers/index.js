
import { combineReducers } from 'redux'

import activeDocuments from './documents'
import registry from './registry'

export default (combineReducers({
  registry, activeDocuments
}));
