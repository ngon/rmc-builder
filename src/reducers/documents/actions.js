// Constants
import {
  INSERT_ELEMENT, SELECT_ELEMENT, DESTROY_ELEMENT, MOVE_ELEMENT
} from 'constants/actions';

import {
  INSERT_DOCUMENT, SELECT_DOCUMENT, DESTROY_DCCUMENT, RENAME_DOCUMENT
} from 'constants/actions';

// Action creators
export function insertElement(path, element) {
  return {
    type: INSERT_ELEMENT,
    payload: { path, element }
  }
}

export function selectElement(path) {
  return {
    type: SELECT_ELEMENT,
    payload: { path }
  };
};

export function destroyElement(path) {
  return {
    type: DESTROY_ELEMENT,
    payload: { path }
  };
};

export function moveElement(fromPath, toPath) {
  return {
    type: MOVE_ELEMENT,
    payload: { fromPath, toPath }
  };
}

export function selectDocument(name) {
  return {
    type: SELECT_DOCUMENT,
    payload: { name }
  };
};
