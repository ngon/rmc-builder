import blog from 'test-data/blog';
import features from 'test-data/features';

export default {
  currentDocument: 'blog',
  documents: {
    blog: {
      content: blog,
      displayName: 'Blog',
      currentElement: null
    },
    features: {
      content: features,
      displayName: 'Features',
      currentElement: null
    }
  }
};
