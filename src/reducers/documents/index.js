export * from './actions';

import eq from 'lodash.isequal';

import {
  INSERT_DOCUMENT, SELECT_DOCUMENT, DESTROY_DCCUMENT, RENAME_DOCUMENT,
  INSERT_ELEMENT, SELECT_ELEMENT, DESTROY_ELEMENT, MOVE_ELEMENT
} from 'constants/actions';

import defaultState from './defaultState';

const selectDocument = ({ documents }, { name }) => ({
  documents, currentDocument: name
});

const selectElement = ({ currentDocument, documents }, { path }) => ({
  currentDocument, documents: {
    ...documents,
    [currentDocument]: {
      ...documents[currentDocument],
      currentElement: path
    }
  }
});

const add = (node, path, element) => path.length === 0 ? ({
  ...node, children: [...node.children, element]
}) : ({
  ...node, children: node.children.map((c, i) => i != path[0] ? c : add(c, path.slice(1), element))
});

const insertElement = ({ currentDocument, documents }, { path, element }) => ({
  currentDocument, documents: {
    ...documents,
    [currentDocument]: {
      ...documents[currentDocument],
      currentElement: [...path, at(documents[currentDocument].content, path).children.length],
      content: add(documents[currentDocument].content, path, element)
    }
  }
});

const at = (node, path) => path.reduce(({ children }, i) => children[i], node)

const remove = (node, path) => typeof node == 'string' ? node : path.length == 1 ? ({
  ...node, children: node.children.filter((_, i) => i != path[0])
}) : ({
  ...node,
  children: node.children.map((c, i) => i != path[0] ? c : remove(c, path.slice(1)))
});

const destroyElement = ({ currentDocument, documents }, { path }) => ({
  currentDocument,
  documents: {
    ...documents,
    [currentDocument]: {
      ...documents[currentDocument],
      currentElement: eq(path, documents[currentDocument].currentElement) ?
        null : documents[currentDocument].currentElement,
      content: remove(documents[currentDocument].content, path)
    }
  }
});

const moveElement = (state, { fromPath, toPath }) => {
  const state1 = destroyElement(state, { path: fromPath });
  return insertElement(state1, {
    path: toPath,
    element: at(state.documents[state.currentDocument].content, fromPath)
  });
};

const id = state => state;

const handlerMap = {
  [SELECT_DOCUMENT]: selectDocument,
  [SELECT_ELEMENT]: selectElement,
  [DESTROY_ELEMENT]: destroyElement,
  [INSERT_ELEMENT]: insertElement,
  [MOVE_ELEMENT]: moveElement
};

export default function(state = defaultState, action = {}) {
  return (handlerMap[action.type] || id)(state, action.payload);
}
