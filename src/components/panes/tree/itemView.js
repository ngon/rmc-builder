import React, { Component, createElement } from 'react'
import stateful from 'react-stateful'
import { DragSource, DropTarget } from 'react-dnd';
import { connect } from 'react-redux'

import { ActionMenu } from './itemMenu'
import { TREE_ITEM } from 'constants/ui';

import { selectElement, destroyElement, moveElement } from 'reducers/documents'

import Panel from 'react-bootstrap/lib/Panel'
import MenuItem from 'react-bootstrap/lib/MenuItem'


const intersperse = (a, b) => [].concat(...a.map((v, i) => [v, b]))
  .slice(0, a.length*2 - 1);

const canExpand = items => items.filter(i => typeof i == 'object').length > 0;

const styles = {
  label: {
    fontFamily: '"Lucida Console", Monaco, monospace',
    color: '#d76b6b'
  },
  bracket: {
    color: '#999'
  },
  gutter: {
    paddingLeft: 10,
    borderLeft: '1px solid #555'
  },
  destroy: {
    float: 'right',
    marginRight: 5,
    cursor: 'pointer'
  }
};

const ToggleState = {
  initialize: props => ({ open: true }),
  reducers: {
    toggle: (props, { open }, ev) =>
      (ev.preventDefault(), ev.stopPropagation(), { open: !open })
  }
};

const DragSpec = {
  beginDrag: props => props,
  endDrag(props, monitor, comp) {
    props.dispatch(moveElement(props.id, monitor.getDropResult().id));
  }
};

const DropSpec = { drop: a => a };

@connect()
@DragSource(TREE_ITEM, DragSpec, c => ({ source: c.dragSource() }))
@DropTarget(TREE_ITEM, DropSpec, c => ({ target: c.dropTarget() }))
@stateful(ToggleState)
export class ItemView extends Component {
  notify(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.props.dispatch(selectElement(this.props.id));
  }
  destroy(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.props.dispatch(destroyElement(this.props.id));
  }
  render() {
    const { label, open, toggle, id, dispatch, source, target, ...rest } = this.props;
    const items = this.props.items || [];
    return (
      <div onClick={e => this.notify(e)} className="tree-item">
        {
          source(target(
            <div className="tree-item-label" style={styles.label}>
              <span onClick={toggle}>
                {!open && canExpand(items) ? '+ ' : false}
                <span style={styles.bracket}>&lt;</span>
                {label}
                <span style={styles.bracket}>
                  {!canExpand(items) ? ' /' : false}&gt;
                </span>
              </span>
              <ActionMenu id={id} />
            </div>
          ))
        }
        {!open ? false :
          <div style={styles.gutter}>
          {
            items.map((i, j) => [i, j])
              .filter(i => typeof i[0] == 'object')
              .map(([{ type, props }, i]) =>
                <ItemView label={type}
                          id={[...id, i]}
                          items={props.children}
                          key={i} />
              )
          }
          </div>
        }
        {!open || !canExpand(items) ? false :
          <div className="tree-item-label-end" style={styles.label}>
            <span onClick={toggle}>
              <span style={styles.bracket}>&lt;/</span>
              {label}
              <span style={styles.bracket}>&gt;</span>
            </span>
          </div>
        }
      </div>
    );
  }
};
