import React, { Component, createElement } from 'react'
import { connect } from 'react-redux'
import { selectElement, destroyElement, insertElement } from 'reducers/documents'
import autobind from 'autobind-decorator'

import stateful from 'react-stateful'

import { ModalForm } from 'components/modalForm'
import { ComponentForm } from 'components/componentForm'
import MenuItem from 'react-bootstrap/lib/MenuItem'
import Input from 'react-bootstrap/lib/Input'

const ToggleState = {
  initialize: props => ({ open: false }),
  reducers: {
    toggle: (props, { open }, ev) => (ev.preventDefault(), ev.stopPropagation(), { open: !open })
  }
};

const styles = {
  panel: {
    float: 'right',
    marginRight: '1.5px',
    marginTop: '1.5px'
  },
  menu: {
    zIndex: 9999999,
    right: 0,
    left: 'auto',
    fontSize: 10
  }
};

@connect()
@stateful(ToggleState)
export class ActionMenu extends Component {
  @autobind
  dismiss(...args) {
    if (this.props.open) {
      this.props.toggle(...args);
    }
  }
  @autobind
  insert(...args) {
    const { toggle, id, dispatch } = this.props;
    this.refs.insert.show().then(element =>
      dispatch(insertElement(id, element))
    );
  }
  render() {
    const { open, toggle, id, dispatch } = this.props;
    const oc = open ? 'open' : '';
    return (
      <div style={styles.panel}
           className={`dropdown ${oc} tree-item-destroy`}
           onClick={toggle}>
        <i style={styles.panel} className={`fa fa-cog`} />
        <ModalForm header="Insert new tag"
                   formClass={ComponentForm}
                   ref="insert" />
        {
          !open ? false :
          <ul style={styles.menu}
              onMouseLeave={this.dismiss}
              className={`dropdown-menu`}>
            <MenuItem header>General</MenuItem>
            <MenuItem onSelect={() => dispatch(selectElement(id))}>
              <u>S</u>elect
            </MenuItem>
            <MenuItem header>Actions</MenuItem>
            <MenuItem>
              <u>M</u>ove
            </MenuItem>
            <MenuItem onSelect={() => dispatch(destroyElement(id))}>
              <u>D</u>elete
            </MenuItem>
            <MenuItem onSelect={this.insert}>
              <u>I</u>nsert
            </MenuItem>
          </ul>
        }
      </div>
    );
  }
}
