import React, { Component } from 'react'

import Well from 'react-bootstrap/lib/Well'
import Panel from 'react-bootstrap/lib/Panel'
import Accordian from 'react-bootstrap/lib/PanelGroup'

import { Scrollbars } from 'components/scrollbar';
import { ItemView } from './itemView'

const styles = {
  panel: {
    maxHeight: '100vh',
    height: '100vh',
    color: '#fff',
    WebkitUserSelect: 'none',
    MozUserSelect: 'none',
    msUserSelect: 'none',
    userSelect: 'none',
    fontSize: 11,
    fontWeight: 200,
    backgroundColor: '#2d2a34',
    margin: 0,
    padding: 0
  },
  header: {
    fontSize: 18
  },
  scroll: {
    width: '100%',
    height: '100vh',
    backgroundColor: '#2d2a34'
  },
  inner: {
    backgroundColor: '#2d2a34',
    marginBottom: 0
  }
};

export class TreePane extends Component {
  render() {
    const { target: { displayName, content: { type, props } } } = this.props;
    return (
      <Well bsSize="small" style={styles.panel}>
        <Scrollbars style={styles.scroll}>
          <Panel style={styles.inner}>
            <div style={styles.header}>Tree View - {displayName}</div>
            <hr />
            <Accordian accordian subheader="Component Explorer">
              <ItemView id={[]} label={type} items={props.children} />
            </Accordian>
          </Panel>
        </Scrollbars>
      </Well>
    );
  }
}
