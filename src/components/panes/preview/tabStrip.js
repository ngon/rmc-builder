import React, { Component } from 'react';
import { connect } from 'react-redux';

import { selectDocument } from 'reducers/documents';

const styles = {
  panel: {
    backgroundColor: '#2d2a34',
    color: '#fff',
    fontSize: 11,
    fontWeight: 200,
    margin: 0,
    display: 'flex',
    height: 26  // Computed height of tabs
  },
  tab: {
    flexGrow: 1,
    textAlign: 'center',
    padding: 5
  },
  active: {
    backgroundColor: '#494455'
  }
};

export class Tab extends Component {
  render() {
    const { onClick, label, active } = this.props;
    return (
      <div style={{ ...styles.tab, ...(active?styles.active:{}) }} onClick={() => onClick(label)}>
        {label}
      </div>
    );
  }
}

@connect(state => state.activeDocuments)
export class TabStrip extends Component {
  render() {
    const { currentDocument, documents, dispatch } = this.props;
    return (
      <div style={{ position: 'relative' }}>
        <div style={styles.panel}>
          {
            Object.keys(documents).map((label, i) =>
              <Tab key={i}
                   active={currentDocument === label}
                   onClick={name => dispatch(selectDocument(name))}
                   label={label} />
            )
          }
        </div>
      </div>
    );
  }
}
