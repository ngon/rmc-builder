import React, { Component } from 'react'
import { render } from 'react-dom'
import { createElement } from 'rmc-core'
import { Registry } from 'registry'

import { TabStrip } from './tabStrip'

const styles = {
  panel: {
    height: '100vh',
    maxHeight: '100vh',
    position: 'relative'
  },
  tabs: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 26
  },
  preview: {
    width: '100%',
    height: 'calc(100vh - 26px)',
    overflowY: 'scroll'
  }
}

export class PreviewPane extends Component {
  render() {
    const { target } = this.props;
    return (
      <div style={styles.panel}>
        <div style={styles.tab}><TabStrip /></div>
        <div style={styles.preview}>{createElement(target, Registry)}</div>
      </div>
    );
  }
}
