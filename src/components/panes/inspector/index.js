import React, { Component } from 'react'
import format from 'json-format'

import Panel from 'react-bootstrap/lib/Panel'

const intersperse = (a, b) => [].concat(...a.map((v, i) => [v, <br key={i} />]))
  .slice(0, a.length*2 - 1);

const styles = {
  panel: {
    maxHeight: '100vh',
    height: '100vh',
    overflowY: 'scroll',
    backgroundColor: '#2d2a34',
    color: '#fff',
    fontSize: 11,
    fontWeight: 200,
    margin: 0
  },
  header: {
    fontSize: 18
  },
  row: {
    display: 'flex',
    margin: '10px 0'
  },
  label: {
    display: 'block',
    width: '30%'
  },
  field: {
    display: 'block',
    width: '70%',
    fontFamily: '"Lucida Console", Monaco, monospace'
  }
};

export class InspectorPane extends Component {
  render() {
    const { target: { content, currentElement } } = this.props;
    const el = currentElement ?
        currentElement.reduce((s, i) => s.props.children[i], content)
      :
        null;
    const { props: { children, ...rest } } = el || { props: {} };
    return (
      <Panel style={styles.panel}>
        <div style={styles.header}>Properties Inspector</div>
        <hr />
        {
          el === null ? 'Nothing is selected' :
          <div>
            <div style={styles.row}>
              <div style={styles.label}>Tag Name: </div>
              <div style={styles.field}>{el.type}</div>
            </div>
            <div style={styles.row}>
              <div style={styles.label}>Properties: </div>
              <div style={styles.field}>{intersperse(format(rest).split('\n'), <br />)}</div>
            </div>
            <div style={styles.row}>
              <div style={styles.label}>Content: </div>
              <div style={styles.field}>{intersperse(format(el.props.children).split('\n'), <br />)}</div>
            </div>
          </div>
        }
      </Panel>
    );
  }
};
