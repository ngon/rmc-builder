import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

class Scroller extends Component {

  static displayName = "Scrollbars"

  render() {
    return (
      <Scrollbars style={this.props.style}>
        {this.props.children}
      </Scrollbars>
    );
  }
}

export { Scroller as Scrollbars };
