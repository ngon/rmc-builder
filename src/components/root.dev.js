
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { MainLayout } from 'components/layout';
import DevTools from './devtools';
import DocumentMeta from 'react-document-meta';
import { DragDropContext } from 'react-dnd';
import Html5Backend from 'react-dnd-html5-backend';

const meta = {
  meta: {
    viewport: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
  }
};

@DragDropContext(Html5Backend)
export default class extends Component {
  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <div>
          <DocumentMeta {...meta} />
          <MainLayout />
          <DevTools />
        </div>
      </Provider>
    );
  }
}
