import React, { Component } from 'react'
import autobind from 'autobind-decorator'

import Button from 'react-bootstrap/lib/Button'
import Modal from 'react-bootstrap/lib/Modal'

export class ModalForm extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      open: false,
      promise: null,
      form: null
    };
  }
  @autobind
  show() {
    if (!this.state.open) {
      return new Promise((resolve, reject) => {
        this.setState({
          open: true,
          promise: { resolve, reject }
        });
      });
    }
  }
  @autobind
  update(form) {
    this.setState({ form });
  }
  @autobind
  submit() {
    const { form, promise: { resolve } } = this.state;
    this.setState({
      open: false,
      promise: null,
      form: null
    }, () => resolve(form));
  }
  @autobind
  cancel() {
    const { promise: { reject } } = this.state;
    this.setState({
      open: false,
      promise: null,
      form: null
    }, reject);
  }
  render() {
    const FormComponent = this.props.formClass;
    return (
      <Modal show={this.state.open} onHide={this.cancel} ref="modal">
        <Modal.Header>
          {this.props.header}
        </Modal.Header>
        <Modal.Body>
          <FormComponent value={this.state.form} onChange={this.update} />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.cancel}>Close</Button>
          <Button onClick={this.submit} bsStyle="primary">Submit</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}
