import React, { Component } from 'react'
import { TreePane } from 'components/panes/tree'
import { PreviewPane } from 'components/panes/preview'
import { InspectorPane } from 'components/panes/inspector'
import { connect } from 'react-redux'

const styles = {
  panel: {
    flexGrow: '1',
    minWidth: 250,
    maxWidth: 380,
    height: '100vh',
    margin: 0
  },
  preview: {
    flexGrow: '10',
    alignSelf: 'stretch',
    height: '100vh'
  }
};

@connect(state => state.activeDocuments)
export class MainLayout extends Component {
  render() {
    const { currentDocument, documents } = this.props;
    const doc = documents[currentDocument];
    return (
      <div style={{ display: 'flex', height: '100vh', overflow: 'hidden' }}>
        <div style={styles.panel}>
          <TreePane target={doc} />
        </div>
        <div style={styles.panel}>
          <InspectorPane target={doc} />
        </div>
        <div style={styles.preview}>
          <PreviewPane target={doc.content} />
        </div>
      </div>
    );
  }
};
