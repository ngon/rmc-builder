import React, { Component } from 'react';
import stateful from 'react-stateful';
import { reducers } from 'helpers/stateful';

import Input from 'react-bootstrap/lib/Input';

const ComponentFormState = {
  initialize: ({ tagName = "", props = {}, children = []}) => ({ tagName, props, children }),
  reducers
}

@stateful(ComponentFormState)
export class ComponentForm extends Component {
  render() {
    const { tagName, props, children, set, tf } = this.props;
    return (
      <div>
        <Input type="text"
               value={tagName}
               label="Tag Name"
               onChange={e => tf('tagName', e)} />
        <Input type="textarea"
               value={JSON.stringify(props)}
               label="Properties"
               onChange={e => set('props', JSON.parse(e.target.value))} />
        <Input type="textarea"
               value={children[0]}
               label="Contents"
               onChange={e => set('children', [e.target.value])} />
      </div>

    )
  }
}
