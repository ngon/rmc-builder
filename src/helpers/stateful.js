// react-stateful helpers

const set = (props, state, name, value) => {
  const delta = { [name]: value };
  props.onChange &&
    props.onChange(Object.assign({}, state, delta));
  return delta;
};

const setMany = (props, state, delta) => {
  props.onChange &&
    props.onChange(Object.assign({}, state, delta));
  return delta;
}

export const reducers = {
  set, setMany,
  tf(props, state, name, ev) {
    return set(props, state, name, ev.target.value);
  },
  cb(props, state, name, ev) {
    return set(props, state, name, ev.target.checked);
  },
  dd(props, state, name, ev) {
    return set(props, state, name, ev.target.menuItem)
  }
}

export const init = (fields) => props =>
  Object.assign({}, fields.reduce((s, f) => (s[f] = '', s), {}), props);
