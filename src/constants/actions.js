export const SELECT_ELEMENT = 'rmc-builder/elements/SELECT_ELEMENT';
export const INSERT_ELEMENT = 'rmc-builder/elements/INSERT_ELEMENT';
export const DESTROY_ELEMENT = 'rmc-builder/elements/DESTROY_ELEMENT';
export const MOVE_ELEMENT = 'rmc-builder/elements/MOVE_ELEMENT';

export const SELECT_DOCUMENT = 'rmc-builder/documents/SELECT_DOCUMENT';
export const INSERT_DOCUMENT = 'rmc-builder/documents/INSERT_DOCUMENT';
export const DESTROY_DOCUMENT = 'rmc-builder/documents/DESTROY_DOCUMENT';
export const RENAME_DOCUMENT = 'rmc-builder/document/RENAME_DOCUMENT';
