const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config');

const server = new WebpackDevServer(webpack(config), config.devServer)
  .listen(config.port, '0.0.0.0', err => err ?
    console.error(err)
  :
    console.log(`Listening at localhost:${config.port}`));
